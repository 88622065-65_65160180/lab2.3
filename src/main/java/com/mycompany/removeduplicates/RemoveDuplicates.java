/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.removeduplicates;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class RemoveDuplicates {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int k = 1; 

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[k - 1]) {
                nums[k] = nums[i];
                k++;
            }
        }

        return k;
    }

    public static void main(String[] args) {
        int[] nums = {1, 1, 2,};

        int k = removeDuplicates(nums);

        System.out.println("Updated Array: " + Arrays.toString(nums));
        System.out.println("Number of unique elements: " + k);
    }
}

